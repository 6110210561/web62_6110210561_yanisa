using NewsReport.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace NewsReport.Data
{
    public class NewsReportContext : IdentityDbContext<NewsUser>
	{
        public DbSet<News> newsList { get; set; }
		public DbSet<NewsCategory> NewsCategory { get; set; }
		
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(@"Data source=NewsReport.db");			
        }
    }
}