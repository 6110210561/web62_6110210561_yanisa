﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

using MEDModern.Models;
using MEDModern.Data;

namespace MEDModern.Pages
{
    public class IndexModel : PageModel
    {
		private readonly MEDModernContext db;
		public IndexModel(MEDModernContext db) => this.db = db;
		public List<Med> meds { get; set; } = new List<Med>();
		
        public void OnGet()
        {
			meds = db.Meds.ToList();
			}
    }
}
