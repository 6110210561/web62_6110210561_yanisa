using MEDModern.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace MEDModern.Data
{
	public class MEDModernContext : IdentityDbContext<NewsUser>
	{
		public DbSet<Med> Meds { get; set; }
		
		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			optionsBuilder.UseSqlite(@"Data source=Med.db");
		}
	}
}